package com.smdslim.sell;

import com.google.gson.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SellingBot {

    private JsonParser parser = new JsonParser();
    private Map<String, ArrayList<String>> config;
    private Map<String, String> cookieData;
    private boolean debug = false;

    int percent = 25;

    public static void main(String[] args) {

        boolean debug = false;
        if (args.length == 1) debug = true;

        SellingBot sellingBot = new SellingBot(debug);


        while (true) {
            int currentStamp = sellingBot.getCurrentStamp();
            int lastExposeStamp = sellingBot.getLastExposeStamp();
            if (lastExposeStamp == 0 || currentStamp - lastExposeStamp >= 30 * 60) {
                sellingBot.removeItems();
//                sellingBot.setLastExposeStamp(currentStamp);
            }
            sellingBot.sellItems();
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                System.out.println(sellingBot.getDateString() + " " + e.getMessage());
            }
        }
    }

    public SellingBot(boolean debug) {
        config = parseConfig();
        cookieData = parseConfig("cookies.xml");
        if (debug) this.debug = debug;
    }

    protected Map<String, ArrayList<String>> parseConfig() {
        Map<String, ArrayList<String>> result = new HashMap<>();
        try {
            File xmlFile = new File("config.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getChildNodes().item(0).getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node tempNode01 = nodeList.item(i);
                if (tempNode01.getNodeType() == Node.ELEMENT_NODE) {
                    NodeList childNodes = tempNode01.getChildNodes();
                    ArrayList<String> children = new ArrayList<>();
                    for (int j = 0; j < childNodes.getLength(); j++) {
                        Node tempNode02 = childNodes.item(j);
                        if (tempNode02.getNodeType() == Node.ELEMENT_NODE) {
                            children.add(tempNode02.getTextContent());
                        }
                    }
                    result.put(tempNode01.getNodeName(), children);
                }
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        }
        return result;
    }

    public Map<String, String> parseConfig(String fileName) {
        Map<String, String> result = new HashMap<>();
        try {
            File xmlFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getChildNodes().item(0).getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node tempNode = nodeList.item(i);
                if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                    result.put(tempNode.getNodeName(), tempNode.getTextContent());
                }
            }
        } catch (ParserConfigurationException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        } catch (SAXException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        } catch (IOException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        }
        return result;
    }

    public void sellItems() {
        config = parseConfig();

        ArrayList<String> inventoryURLs = config.get("inventory_urls");
        for (int i = 0; i < inventoryURLs.size(); i++) {
            String url = inventoryURLs.get(i);
            String gameID = parseString(url, ".+/(\\d+)/\\d+$");
            String contextID = parseString(url, ".+/(\\d+)$");
            if (!gameID.equals("") && !contextID.equals("")) {
                Map<String, Map<String, String>> itemsToSell = getLotsToSell(url);
                for (String key : itemsToSell.keySet()) {
                    try {
                        url = "http://steamcommunity.com/market/listings/" + gameID + "/" + URLEncoder.encode(itemsToSell.get(key).get("market_hash_name"), "UTF-8").replace("+", "%20") + "/render/?query=&search_descriptions=0&start=0&count=10";
                        String json = getHTML(url);
                        String html = new JSONObject(json).getString("results_html");
                        org.jsoup.nodes.Document doc = Jsoup.parse(html, "UTF-8");
                        Elements divs = doc.getElementsByClass("market_listing_price_without_fee");
                        ArrayList<Integer> prices = getLastPrices(divs);
                        System.out.print(getDateString() + " Selling " + itemsToSell.get(key).get("market_hash_name") + "; ");
                        if (prices.size() > 8) {
                            String avgPrice = calculateAveragePrice(prices);
                            String endPrice = calculateEndPrice(avgPrice);
                            System.out.print("Average price: " + avgPrice + "; End price: " + endPrice + " ... ");
                            HashMap<String, String> responseData = sellItem(gameID, contextID, key, endPrice);
                            if (responseData.get("responseCode") != null) {
                                if (responseData.get("responseCode").equals("200")) {
                                    System.out.println("Done!");
                                } else {
                                    System.out.println("Failed! | Response code: " + responseData.get("responseCode"));
                                }
                            } else {
                                System.out.println("Error getting response");
                            }
                        } else {
                            System.out.println("Not enough prices amount to calculate precise average price ... ");
                        }
                        Thread.sleep(10000);
                    } catch (UnsupportedEncodingException e) {
                        System.out.println(getDateString() + " " + e.getMessage());
                    } catch (JSONException e) {
                        System.out.println(getDateString() + " " + e.getMessage());
                    } catch (InterruptedException e) {

                    }
                }
            }
        }
    }

    public String parseString(String text, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        String result = "";
        if (m.find()) {
            result = m.group(1);
        }
        return result;
    }

    protected Map<String, Map<String, String>> getLotsToSell(String url) {
        Map<String, Map<String, String>> result = new HashMap<>();
        Map<String, Map<String, String>> inventory = getInventory(url);

        try {
            for (String key : inventory.keySet()) {
                Map<String, String> tempMap = inventory.get(key);
                if (tempMap.get("marketable").equals("1") &&
                        tempMap.get("market_name").equals("Treasure Key") == false &&
                        tempMap.get("market_hash_name").equals("Treasure Key") == false) {

                    if (config.get("not_to_sell_item_ids").contains(tempMap.get("id")) == false) {
                        if (config.get("force_sell_item_ids").contains(tempMap.get("id"))) {
                            result.put(key, tempMap);
                        } else {
                            if (config.get("not_to_sell_item_names").contains(tempMap.get("market_name")) == false &&
                                    config.get("not_to_sell_hero_names").contains(tempMap.get("hero_name")) == false) {
                                result.put(key, tempMap);
                            } else if (config.get("force_sell_item_names").contains(tempMap.get("market_name")) &&
                                    config.get("not_to_sell_item_names").contains(tempMap.get("market_name")) == false) {
                                result.put(key, tempMap);
                            } else if (config.get("not_to_sell_hero_names").contains(tempMap.get("hero_name")) == false) {
                                result.put(key, tempMap);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(getDateString() + " " + e.getMessage());
        }
        return result;
    }

    public String getHTML(String urlToRead) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.addRequestProperty("Cookie", formatCookies());

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();

        } catch (IOException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        } catch (Exception e) {
            System.out.println(getDateString() + " " + e.getMessage());
        }
        return result;
    }

    public ArrayList<Integer> getLastPrices(Elements divs) {
        ArrayList<Integer> result = new ArrayList<>();
        if (!divs.isEmpty()) {
            for (Element div : divs) {
                String str = parseString(div.text(), "(.+)\\s").replaceAll(",", ".");
                if (!str.equals("")) {
                    int price = 0;
                    try {
                        price = (int) (Double.parseDouble(str) * 100);
                    } catch (NumberFormatException e) {
                        System.out.println(getDateString() + " " + e.getMessage());
                    }
                    result.add(price);
                }
            }
        }
        return result;
    }

    public String calculateAveragePrice(ArrayList<Integer> prices) {
        int sum = 0, result;
        if (prices.size() == 10) {
            prices.remove(9);
            prices.remove(8);
        } else if (prices.size() == 9) {
            prices.remove(8);
        }
        int amount = prices.size();
        for (int i = 0; i < amount; i++) {
            sum += prices.get(i);
        }
        result = sum / amount;
        return Integer.toString(result);
    }

    public String calculateEndPrice(String avgPrice) {
        int price = Integer.parseInt(avgPrice);
        double res = (double) price * ((double) percent / 100);
        return Integer.toString(price - (int) res);
    }

    public HashMap<String, String> sellItem(String gameID, String contextID, String id, String price) {
        cookieData = parseConfig("cookies.xml");
        String url = "https://steamcommunity.com/market/sellitem";
        String postData = "sessionid=" + cookieData.get("sessionid") + "&appid=" + gameID + "&contextid=" + contextID + "&assetid=" + id + "&amount=1&price=" + price;
        return sendRequest(url, postData);
    }

    private Map<String, Map<String, String>> getInventory(String url) {
        JsonElement jsonElement;
        JsonObject jsonObject;
        List<String> lots;
        List<String> classes;
        Map<String, Map<String, String>> inventory = new HashMap<>();
        String json = getJSON(url);

        try {
            JSONObject obj = new JSONObject(json);

            if (obj.get("rgDescriptions") instanceof JSONObject && obj.get("rgInventory") instanceof JSONObject) {

                JSONObject rgDescriptions = obj.getJSONObject("rgDescriptions");
                JSONObject rgInventory = obj.getJSONObject("rgInventory");

                jsonElement = parser.parse(new StringReader(rgDescriptions.toString()));
                jsonObject = jsonElement.getAsJsonObject();
                classes = readJSON(jsonObject);

                jsonElement = parser.parse(new StringReader(rgInventory.toString()));
                jsonObject = jsonElement.getAsJsonObject();
                lots = readJSON(jsonObject);

                String classID, lotID;
                JSONObject tempJSONObject;
                for (int i = 0; i < lots.size(); i++) {
                    lotID = lots.get(i);
                    tempJSONObject = rgInventory.getJSONObject(lotID);
                    classID = tempJSONObject.getString("classid");

                    Map<String, String> tempMap = new HashMap<String, String>();
                    tempMap.put("id", lotID);
                    tempMap.put("class_id", classID);
                    tempMap.put("instance_id", tempJSONObject.getString("instanceid"));

                    inventory.put(lotID, tempMap);
                }

                String index, heroName;
                JSONArray tempJSONArray;
                for (int i = 0; i < classes.size(); i++) {
                    index = classes.get(i);
                    tempJSONObject = rgDescriptions.getJSONObject(index);
                    classID = tempJSONObject.getString("classid");

                    for (String key : inventory.keySet()) {
                        Map<String, String> tempMap = inventory.get(key);
                        if (tempMap.get("class_id").equals(classID)) {
                            heroName = "undefined";
                            tempJSONArray = tempJSONObject.getJSONArray("tags");
                            try {
                                heroName = tempJSONArray.getJSONObject(4).getString("name");
                            } catch (JSONException e) {
//                                System.out.println(getDateString() + " " + e.getMessage());
                            }
                            tempMap.put("hero_name", heroName);
                            try {
                                tempMap.put("market_name", tempJSONObject.getString("market_name"));
                            } catch (JSONException e) {
                                System.out.println(getDateString() + " market_name not found");
                                tempMap.put("market_name", "undefined");
                            }
                            try {
                                tempMap.put("market_hash_name", tempJSONObject.getString("market_hash_name"));
                            } catch (JSONException e) {
                                System.out.println(getDateString() + " market_hash_name not found");
                                tempMap.put("market_hash_name", "undefined");
                            }
                            tempMap.put("marketable", Integer.toString(tempJSONObject.getInt("marketable")));
                        }
                    }
                }
            }
        } catch (JSONException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        } catch (JsonIOException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        } catch (JsonSyntaxException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        }

        if (this.debug) {
            System.out.println("Inventory for url: " + url);
            System.out.println(inventory);
        }

        return inventory;
    }

    public String formatCookies() {
        String result = "";
        for (String key : cookieData.keySet()) {
            result += key + "=" + cookieData.get(key) + "; ";
        }
        return result;
    }

    public HashMap<String, String> sendRequest(String urlToRead, String postData) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String responseData = "";
        HashMap<String, String> result = new HashMap<>();

        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Host", "steamcommunity.com");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0");
            conn.setRequestProperty("Accept", "*/*");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            conn.setRequestProperty("Referer", "http://steamcommunity.com/id/smdslim/inventory?modal=1&market=1");
            conn.setRequestProperty("Content-Length", Integer.toString(postData.length()));
            conn.setRequestProperty("Origin", "http://steamcommunity.com");
            conn.addRequestProperty("Cookie", this.formatCookies());
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setRequestProperty("Pragma", "no-cache");
            conn.setRequestProperty("Cache-Control", "no-cache");

            conn.setDoOutput(true);
            conn.setDoInput(true);

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();

            int responseCode = 0;
            responseCode = conn.getResponseCode();

            result.put("responseCode", Integer.toString(responseCode));
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                responseData += line;
            }
            result.put("responseData", responseData);
            rd.close();
        } catch (IOException e) {
//            System.out.println(getDateString() + "; " + e.getMessage());
        } catch (Exception e) {
//            System.out.println(getDateString() + "; " + e.getMessage());
        }
        return result;
    }

    private String getJSON(String urlToRead) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();
        } catch (IOException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        } catch (Exception e) {
            System.out.println(getDateString() + " " + e.getMessage());
        }
        return result;
    }

    private List<String> readJSON(JsonObject jsonObject) {
        List<String> result = new ArrayList<String>();
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            String key = entry.getKey();
            if (jsonObject.isJsonObject())
                result.add(key);
        }
        return result;
    }

    public void removeItems() {
        ArrayList<String> items = scanItemsToRemove();
        int amount = items.size();
        for (int i = 0; i < amount; i++) {
            System.out.print(getDateString() + " Removing: " + items.get(i) + " ... ");
            HashMap<String, String> responseData = removeItem(items.get(i));
            if (responseData.get("responseCode") != null) {
                if (responseData.get("responseCode").equals("200")) {
                    System.out.println("Done!");
                } else {
                    System.out.println("Failed! | Response code: " + responseData.get("responseCode"));
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {

                }
            } else {
                System.out.println("Error getting response");
            }
        }
    }

    public ArrayList<String> scanItemsToRemove() {
        ArrayList<String> result = new ArrayList<>();
        Map<String, String> items = getExposedItems();

        for (String key : items.keySet()) {
            String html = getHTML(items.get(key));
            org.jsoup.nodes.Document doc = Jsoup.parse(html, "UTF-8");
            Element searchResultsRows = doc.getElementById("searchResultsRows");
            if (searchResultsRows != null) {
                Elements divs = searchResultsRows.getElementsByClass("market_listing_row");
                if (!divs.isEmpty()) {
                    int position = 0;
                    String itemName = "";
                    for (Element div : divs) {
                        position++;
                        String itemID = parseString(div.attr("id"), "listing_(.+)");
                        if (!itemID.equals("") && itemID.equals(key)) {
                            itemName = div.getElementById("listing_" + itemID + "_name").text();
                            break;  // Found element position, breaking loop
                        }
                    }
                    if (position >= 3 || position == 0) {
                        System.out.println(getDateString() + " Adding to remove list. ID: " + key + "; itemName: " + itemName + "; Position: " + position);
                        result.add(key);
                    }
                }
            }
        }
        return result;
    }

    public HashMap<String, String> removeItem(String id) {
        cookieData = parseConfig("cookies.xml");
        String url = "http://steamcommunity.com/market/removelisting/" + id;
        String postData = "sessionid=" + cookieData.get("sessionid");
        return sendRequest(url, postData);
    }

    public Map<String, String> getExposedItems() {
        Map<String, String> items = new HashMap<>();
        String html = getHTML("http://steamcommunity.com/market");
        org.jsoup.nodes.Document doc = Jsoup.parse(html, "UTF-8");
        Element listings = doc.getElementById("myListings");
        if (listings != null) {
            Elements spans = listings.getElementsByClass("market_listing_item_name");
            if (!spans.isEmpty()) {
                for (Element span : spans) {
                    String itemID = parseString(span.attr("id"), "mylisting_(.+)_name");
                    if (!itemID.equals("")) {
                        Elements links = span.getElementsByClass("market_listing_item_name_link");
                        items.put(itemID, links.first().attr("href"));
                    }
                }
            }
        }
        return items;
    }

    public int getCurrentStamp() {
        long epoch = System.currentTimeMillis() / 1000;
        return (int) epoch;
    }

    public void setLastExposeStamp(int stamp) {
        try {
            File file = new File("last_expose");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(Integer.toString(stamp));
            bw.close();
        } catch (IOException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        }
    }

    public int getLastExposeStamp() {
        String line, result = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File("last_expose")));
            while ((line = reader.readLine()) != null) {
                result += line;
            }
        } catch (FileNotFoundException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        } catch (IOException e) {
            System.out.println(getDateString() + " " + e.getMessage());
        }
        return Integer.parseInt(result);
    }

    public String getDateString() {
        DateFormat df = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
        String date = df.format(new Date());
        return date;
    }
}


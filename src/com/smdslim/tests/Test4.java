package com.smdslim.tests;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*
    Gets exposed items, checks their position relatively to other lots
 */


public class Test4 extends Test3 {

    public static void main(String[] args) {
        Test4 test4 = new Test4();
        test4.removeItems();


    }

    public int getCurrentStamp() {
        long epoch = System.currentTimeMillis() / 1000;
        return (int) epoch;
    }

    public void setLastExposeStamp(int stamp) {
        try {
            File file = new File("last_expose");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(Integer.toString(stamp));
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getLastExposeStamp() {
        String line, result = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File("last_expose")));
            while ((line = reader.readLine()) != null) {
                result += line;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(result);
    }

    public void sellItems() {
        ArrayList<String> inventoryURLs = config.get("inventory_urls");
        for (int i = 0; i < inventoryURLs.size(); i++) {
            String url = inventoryURLs.get(i);
            String gameID = parseString(url, ".+/(\\d+)/\\d+$");

            String contextID = parseString(url, ".+/(\\d+)$");
            if (!gameID.equals("") && !contextID.equals("")) {
                Map<String, Map<String, String>> itemsToSell = getLotsToSell(url);
                for (String key : itemsToSell.keySet()) {
                    try {
                        url = "http://steamcommunity.com/market/listings/" + gameID + "/" + URLEncoder.encode(itemsToSell.get(key).get("market_name"), "UTF-8").replace("+", "%20") + "/render/?query=&search_descriptions=0&start=0&count=10";
                        String json = getHTML(url);
                        String html = new JSONObject(json).getString("results_html");
                        Document doc = Jsoup.parse(html, "UTF-8");
                        Elements divs = doc.getElementsByClass("market_listing_their_price");
                        ArrayList<Integer> prices = getLastPrices(divs);
                        System.out.println(itemsToSell.get(key).get("market_name"));
                        System.out.println("Prices amount: " + prices.size());
                        if (prices.size() > 8) {
                            System.out.println(prices);
                            String avgPrice = calculateAveragePrice(prices);
                            System.out.println("Average price: " + avgPrice);
                            String endPrice = calculateEndPrice(avgPrice);
                            System.out.println("End price: " + endPrice);
                            System.out.println("Selling " + itemsToSell.get(key).get("market_name"));
                            System.out.println(sellItem(gameID, contextID, key, endPrice));
                        } else {
                            System.out.println("Not enough prices amount to calculate presice average price ... ");
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void removeItems() {
        ArrayList<String> items = scanItemsToRemove();
        int amount = items.size();
        for (int i = 0; i < amount; i++) {
            System.out.println("Removing: " + items.get(i));
            System.out.println(removeItem(items.get(i)));
        }
    }

    public ArrayList<String> scanItemsToRemove() {
        ArrayList<String> result = new ArrayList<>();
        Map<String, String> items = getExposedItems();

        for (String key : items.keySet()) {
            String html = getHTML(items.get(key));
            Document doc = Jsoup.parse(html, "UTF-8");
            Element searchResultsRows = doc.getElementById("searchResultsRows");
            Elements divs = searchResultsRows.getElementsByClass("market_listing_row");
            if (!divs.isEmpty()) {
                int position = 0;
                for (Element div : divs) {
                    position++;
                    String itemID = parseString(div.attr("id"), "listing_(.+)");
                    if (!itemID.equals("") && itemID.equals(key)) {
                        break;  // Found element position, breaking loop
                    }
                }
                if (position >= 3 || position == 0) {
                    System.out.println("Adding to remove list. ID: " + key + "; Href: " + items.get(key) + "; Position: " + position);
                    result.add(key);
                }
            }
        }
        return result;
    }

    public String calculateEndPrice(String avgPrice) {
        int price = Integer.parseInt(avgPrice);
        double res = (double) price * ((double) percent / 100);
        return Integer.toString(price - (int) res);
    }

    public String calculateAveragePrice(ArrayList<Integer> prices) {
        int sum = 0, amount = prices.size(), result;
        for (int i = 0; i < amount; i++) {
            sum += prices.get(i);
        }
        Double dbl = ((double) sum / (double) amount);
        result = sum / amount;
        return Integer.toString(result);
    }

    public ArrayList<Integer> getLastPrices(Elements divs) {
        ArrayList<Integer> result = new ArrayList<>();
        for (Element div : divs) {
            Elements span = div.getElementsByClass("market_listing_price_without_fee");
            if (!span.isEmpty()) {
                String str = parseString(span.first().text(), "(.+)\\s").replaceAll(",", ".");
                if (!str.equals("")) {
                    int price = (int) (Double.parseDouble(str) * 100);
                    result.add(price);
                }
            }
        }
        return result;
    }

    public Map<String, String> getExposedItems() {
        String html = getHTML("http://steamcommunity.com/market");
        Document doc = Jsoup.parse(html, "UTF-8");
        Element listings = doc.getElementById("myListings");
        Elements spans = listings.getElementsByClass("market_listing_item_name");
        Map<String, String> items = new HashMap<>();
        for (Element span : spans) {
            String itemID = parseString(span.attr("id"), "mylisting_(.+)_name");
            if (!itemID.equals("")) {
                Elements links = span.getElementsByClass("market_listing_item_name_link");
                items.put(itemID, links.first().attr("href"));
            }
        }
        return items;
    }

    public String parseString(String text, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        String result = "";
        if (m.find()) {
            result = m.group(1);
        }
        return result;
    }

    public String getHTML(String urlToRead) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.addRequestProperty("Cookie", formatCookies());

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
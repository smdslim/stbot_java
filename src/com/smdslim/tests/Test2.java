package com.smdslim.tests;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.lang.model.element.Element;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.File;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.Map.Entry;

/*

    Gets items list from inventory to sell

 */

public class Test2 {

    private JsonParser parser = new JsonParser();
    protected Map<String, ArrayList<String>> config;

    public Test2() {
        config = parseConfig();
    }

    private Map<String, Map<String, String>> getInventory(String url) {
        JsonElement jsonElement;
        JsonObject jsonObject;
        List<String> lots;
        List<String> classes;
        Map<String, Map<String, String>> inventory = new HashMap<String, Map<String, String>>();

        String json = getJSON(url);

        JSONObject obj = new JSONObject(json);
        JSONObject rgDescriptions = obj.getJSONObject("rgDescriptions");
        JSONObject rgInventory = obj.getJSONObject("rgInventory");

        jsonElement = parser.parse(new StringReader(rgDescriptions.toString()));
        jsonObject = jsonElement.getAsJsonObject();
        classes = readJSON(jsonObject);

        jsonElement = parser.parse(new StringReader(rgInventory.toString()));
        jsonObject = jsonElement.getAsJsonObject();
        lots = readJSON(jsonObject);

        String classID, lotID;
        JSONObject tempJSONObject;
        for (int i = 0; i < lots.size(); i++) {
            lotID = lots.get(i);
            tempJSONObject = rgInventory.getJSONObject(lotID);
            classID = tempJSONObject.getString("classid");

            Map<String, String> tempMap = new HashMap<String, String>();
            tempMap.put("id", lotID);
            tempMap.put("class_id", classID);
            tempMap.put("instance_id", tempJSONObject.getString("instanceid"));

            inventory.put(lotID, tempMap);
        }

        String index, heroName;
        JSONArray tempJSONArray;
        for (int i = 0; i < classes.size(); i++) {
            index = classes.get(i);
            tempJSONObject = rgDescriptions.getJSONObject(index);
            classID = tempJSONObject.getString("classid");

            for (String key : inventory.keySet()) {
                Map<String, String> tempMap = inventory.get(key);
                if (tempMap.get("class_id").equals(classID)) {
                    heroName = "undefined";
                    tempJSONArray = tempJSONObject.getJSONArray("tags");
                    try {
                        heroName = tempJSONArray.getJSONObject(4).getString("name");
                    } catch (JSONException e) {
                    }
                    tempMap.put("hero_name", heroName);
                    try {
                        tempMap.put("market_name", tempJSONObject.getString("market_name"));
                    } catch (JSONException e) {
                        System.out.println("market_name not found");
                        tempMap.put("market_name", "undefined");
                    }
                    try {
                        tempMap.put("market_hash_name", tempJSONObject.getString("market_hash_name"));
                    } catch (JSONException e) {
                        System.out.println("market_hash_name not found");
                        tempMap.put("market_hash_name", "undefined");
                    }
                    tempMap.put("marketable", Integer.toString(tempJSONObject.getInt("marketable")));
                }
            }
        }
        return inventory;
    }

    protected Map<String, Map<String, String>> getLotsToSell(String url) {
        Map<String, Map<String, String>> result = new HashMap<>();
        Map<String, Map<String, String>> inventory = getInventory(url);

        for (String key : inventory.keySet()) {
            Map<String, String> tempMap = inventory.get(key);
            if (tempMap.get("marketable").equals("1") &&
                    tempMap.get("market_name").equals("Treasure Key") == false &&
                    tempMap.get("market_hash_name").equals("Treasure Key") == false) {

                if (config.get("not_to_sell_item_ids").contains(tempMap.get("id")) == false) {
                    if (config.get("force_sell_item_ids").contains(tempMap.get("id"))) {
                        result.put(key, tempMap);
                    } else {
                        if (config.get("not_to_sell_item_names").contains(tempMap.get("market_name")) == false &&
                                config.get("not_to_sell_hero_names").contains(tempMap.get("hero_name")) == false) {
                            result.put(key, tempMap);
                        } else if (config.get("force_sell_item_names").contains(tempMap.get("market_name")) &&
                                config.get("not_to_sell_item_names").contains(tempMap.get("market_name")) == false) {
                            result.put(key, tempMap);
                        } else if (config.get("not_to_sell_hero_names").contains(tempMap.get("hero_name")) == false) {
                            result.put(key, tempMap);
                        }
                    }
                }
            }
        }
        return result;
    }

    private List<String> readJSON(JsonObject jsonObject) {
        List<String> result = new ArrayList<String>();
        for (Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            String key = entry.getKey();
            if (jsonObject.isJsonObject())
                result.add(key);
        }
        return result;
    }

    private String getJSON(String urlToRead) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getJSON(File file) {
        String line, result = "";
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader rd = new BufferedReader(fileReader);
            while ((line = rd.readLine()) != null) {
                result += line;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getJSON() {
        return "{\"fraudwarnings\": [\"This is a restricted gift which <u>can not be redeemed<\\/u> in these countries: <span class=\\\"fraud_warning_sub_text\\\">Japan, Korea, Republic of, Taiwan, China, Hong Kong<\\/span>\"]}";
    }

    protected Map<String, ArrayList<String>> parseConfig() {
        Map<String, ArrayList<String>> result = new HashMap<>();
        try {
            File xmlFile = new File("config.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getChildNodes().item(0).getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node tempNode01 = nodeList.item(i);
                if (tempNode01.getNodeType() == Node.ELEMENT_NODE) {
                    NodeList childNodes = tempNode01.getChildNodes();
                    ArrayList<String> children = new ArrayList<>();
                    for (int j = 0; j < childNodes.getLength(); j++) {
                        Node tempNode02 = childNodes.item(j);
                        if (tempNode02.getNodeType() == Node.ELEMENT_NODE) {
                            children.add(tempNode02.getTextContent());
                        }
                    }
                    result.put(tempNode01.getNodeName(), children);
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}

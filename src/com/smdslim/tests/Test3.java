package com.smdslim.tests;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/*
    Sells items
    Removes items
 */
public class Test3 extends Test2 {

    Map<String, String> cookieData;
    int percent = 25;

    public static void main(String[] args) {

        String assetID = args[0];

        Test3 test3 = new Test3();
        System.out.println(test3.removeItem(assetID));

    }

    public Test3() {
        cookieData = parseConfig("cookies.xml");
    }

    public String sellItem(String gameID, String contextID, String id, String price) {
        cookieData = parseConfig("cookies.xml");
        String url = "https://steamcommunity.com/market/sellitem";
        String postData = "sessionid=" + cookieData.get("sessionid") + "&appid=" + gameID + "&contextid=" + contextID + "&assetid=" + id + "&amount=1&price=" + price;
        String data = sendRequest(url, postData);
        return data;
    }

    public String removeItem(String id) {
        cookieData = parseConfig("cookies.xml");
        String url = "http://steamcommunity.com/market/removelisting/" + id;
        String postData = "sessionid=" + cookieData.get("sessionid");
        String data = sendRequest(url, postData);
        return data;
    }

    public String sendRequest(String urlToRead, String postData) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";

        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Host", "steamcommunity.com");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0");
            conn.setRequestProperty("Accept", "*/*");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            conn.setRequestProperty("Referer", "http://steamcommunity.com/id/smdslim/inventory?modal=1&market=1");
            conn.setRequestProperty("Content-Length", Integer.toString(postData.length()));
            conn.setRequestProperty("Origin", "http://steamcommunity.com");
            conn.addRequestProperty("Cookie", this.formatCookies());
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setRequestProperty("Pragma", "no-cache");
            conn.setRequestProperty("Cache-Control", "no-cache");

            conn.setDoOutput(true);
            conn.setDoInput(true);

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();
            System.out.println("Response code: " + responseCode);

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String formatCookies() {
        String result = "";

        for (String key : cookieData.keySet()) {
            result += key + "=" + cookieData.get(key) + "; ";
        }
        return result;
    }

    public Map<String, String> parseConfig(String fileName) {
        Map<String, String> result = new HashMap<>();
        try {
            File xmlFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getChildNodes().item(0).getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node tempNode = nodeList.item(i);
                if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                    result.put(tempNode.getNodeName(), tempNode.getTextContent());
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}



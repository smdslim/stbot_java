package com.smdslim.ballance;

import com.smdslim.tests.Test4;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Created by sergeialekseev on 19/02/14.
 * Gets current's account balance
 */
public class GetBallance extends Test4 {
    public static void main(String[] args) {
        GetBallance getBallance = new GetBallance();
        String html = getBallance.getHTML("http://steamcommunity.com/market");
        Document doc = Jsoup.parse(html, "UTF-8");
        Element balanceSpan = doc.getElementById("marketWalletBalanceAmount");
        Element itemAmountSpan = doc.getElementById("my_market_activelistings_number");
        System.out.println("Balance: " + balanceSpan.text() + "; Lots amount: " + itemAmountSpan.text());
    }
}

package com.smdslim.buy;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.text.NumberFormatter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BuyingBot {

    Map<String, String> cookieData;
    int maxPriceToBuy = 300;

    public static void main(String[] args) {
        BuyingBot buyingBot = new BuyingBot();
        while (true) {
            buyingBot.scan();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void scan() {
        String folder = "../stbot_node/ids";
        File actual = new File(folder);
        for (File f : actual.listFiles()) {
            String fileName = f.getName();
            if (fileName.equals(".gitkeep")) {
                continue;
            }
            String json = getJSON(new File(folder + "/" + fileName));
            String url = null;
            String itemName = null;
            Element div = null;

            try {
                JSONObject obj = new JSONObject(json);
                url = obj.getString("url");
                itemName = obj.getString("itemName");
                String html = getHTML(url);
                org.jsoup.nodes.Document doc = Jsoup.parse(html, "UTF-8");
                div = doc.getElementById("listing_" + fileName);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (div != null) {
                Elements withFeeSpan = div.getElementsByClass("market_listing_price_with_fee");
                Elements withoutFeeSpan = div.getElementsByClass("market_listing_price_without_fee");

                if (!withFeeSpan.isEmpty() && !withFeeSpan.isEmpty()) {
                    int withoutFeePrice, withFeePrice, fee;

                    withFeePrice = getPrice(withFeeSpan.first().text());
                    withoutFeePrice = getPrice(withoutFeeSpan.first().text());
                    fee = withFeePrice - withoutFeePrice;

                    if (withFeePrice != 0 && withoutFeePrice != 0 && withoutFeePrice <= maxPriceToBuy) {
                        cookieData = parseConfig("cookies.xml");

                        url = "https://steamcommunity.com/market/buylisting/" + fileName;
                        String postData =
                                "sessionid=" + cookieData.get("sessionid") +
                                        "&currency=5" +
                                        "&subtotal=" + withoutFeePrice +
                                        "&fee=" + fee +
                                        "&total=" + withFeePrice;
                        System.out.print(getDateString() + " Buying " + itemName + "; Price with fee: " + withFeePrice + " ... ");
                        HashMap<String, String> responseData = sendRequest(url, postData);
                        if (responseData.get("responseCode") != null) {
                            if (responseData.get("responseCode").equals("200")) {
                                System.out.println("Done!");
                            } else {
                                System.out.println("Failed! | Response code: " + responseData.get("responseCode"));
                            }
                        } else {
                            System.out.println("Error getting response!");
                        }
                    } else {
                        System.out.println(getDateString() + " " + itemName + " Fail to figure prices or price is very high :" + withoutFeePrice);
                    }
                }
            } else {
                System.out.println(getDateString() + " Item not found: " + itemName + "; ID: " + fileName);
            }
            f.delete();
        }
    }

    public int getPrice(String str) {
        int result = 0;
        String price = parseString(str, "(.+)\\s").replaceAll(",", ".");
        if (!price.equals("")) {
            double d = Double.parseDouble(price) * 100;
            DecimalFormat f = new DecimalFormat("#");
            try {
                result = Integer.parseInt(f.format(d));
            } catch (NumberFormatException e) {
                System.out.println(getDateString() + " " + e.getMessage());
            }
        }
        return result;
    }

    public String parseString(String text, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        String result = "";
        if (m.find()) {
            result = m.group(1);
        }
        return result;
    }

    public BuyingBot() {
        cookieData = parseConfig("cookies.xml");
    }

    public Map<String, String> parseConfig(String fileName) {
        Map<String, String> result = new HashMap<>();
        try {
            File xmlFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getChildNodes().item(0).getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node tempNode = nodeList.item(i);
                if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                    result.put(tempNode.getNodeName(), tempNode.getTextContent());
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String formatCookies() {
        String result = "";
        for (String key : cookieData.keySet()) {
            result += key + "=" + cookieData.get(key) + "; ";
        }
        return result;
    }

    public HashMap<String, String> sendRequest(String urlToRead, String postData) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String responseData = "";
        HashMap<String, String> result = new HashMap<>();

        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Host", "steamcommunity.com");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0");
            conn.setRequestProperty("Accept", "*/*");
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            conn.setRequestProperty("Referer", "http://steamcommunity.com/id/smdslim/inventory?modal=1&market=1");
            conn.setRequestProperty("Content-Length", Integer.toString(postData.length()));
            conn.setRequestProperty("Origin", "http://steamcommunity.com");
            conn.addRequestProperty("Cookie", this.formatCookies());
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setRequestProperty("Pragma", "no-cache");
            conn.setRequestProperty("Cache-Control", "no-cache");

            conn.setDoOutput(true);
            conn.setDoInput(true);

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();
            result.put("responseCode", Integer.toString(responseCode));

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                responseData += line;
            }
            result.put("responseData", responseData);
            rd.close();
        } catch (IOException e) {
//            System.out.println(getDateString() + "; " + e.getMessage());
        } catch (Exception e) {
//            System.out.println(getDateString() + "; " + e.getMessage());
        }
        return result;
    }

    private String getJSON(File file) {
        String line, result = "";
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader rd = new BufferedReader(fileReader);
            while ((line = rd.readLine()) != null) {
                result += line;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getHTML(String urlToRead) {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line;
        String result = "";
        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.addRequestProperty("Cookie", formatCookies());

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getDateString() {
        DateFormat df = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
        String date = df.format(new Date());
        return date;
    }
}
